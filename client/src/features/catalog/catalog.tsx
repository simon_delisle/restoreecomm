import { useState, useEffect } from "react";
import { Product } from "../../app/models/product"
import ProductList from "./ProductList";
import agent from "../../app/api/agent";
import LoadingComponent from "../../app/layout/LoadingComponent";

export default function Catalog(){ //destructuring by specifying the properties of the object or method
    const [products, setProducts] = useState<Product[]>([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
      agent.Catalog.list()
        .then(products => setProducts(products))
        .catch(error => console.log(error))
        .finally(()=> setLoading(false))
    }, []) //empty array dependencies, prevent the infinite loop. Will only be called once.
  
    if (loading) return <LoadingComponent message="loading products..."/>

    return(
        // <> equivalent of using <Fragment> from react without having to import.
        <> 
           <ProductList products={products}/>
        </>
    )
}